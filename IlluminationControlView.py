from labthings import fields
from labthings.views import ActionView

## Extension views
class IlluminationAPIView(ActionView):
    args = {
        "epi": fields.Integer(
            required=True, metadata={"example": 50, "description": "Epi-Illumination Intensity Level (0 to 100)"}
        ),    
        "trans": fields.Integer(
            required=True, metadata={"example": 50, "description": "Trans-Illumination Intensity Level (0 to 100)"}
        ),        
    }

    def post(self, args):      
        # call method to set the new intensity levels
        return self.extension.illumination(args["epi"], args["trans"])

