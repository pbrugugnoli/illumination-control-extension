import logging

from labthings import current_action, fields, find_component, find_extension
from labthings.extensions import BaseExtension
from labthings.views import ActionView, PropertyView, View

# Used to convert our GUI dictionary into a complete eV extension GUI
from openflexure_microscope.api.utilities.gui import build_gui

# Used for get/set illumination intensity
from sangaboard import Sangaboard
from openflexure_microscope.stage.base import BaseStage

# Create the extension class
class IlluminationExtension(BaseExtension):
    # Superclass init function
    def __init__(self):
        super().__init__("org.openflexure.illumination", version="0.0.0")
        self._epi = 0
        self._trans = 0

        # Add our API views
        self.add_view(IlluminationAPIView, "/illumination")

        # Add our GUI description
        gui_description = {
	    "viewPanel": "stream",
            "icon": "flare",
            "forms": [
                {
                    "name": "Set Illumination Power",
                    "route": "/illumination",
                    "isTask": True,
                    "isCollapsible": False,
                    "submitLabel": "Set Illumination",
                    "schema": [
                        {
                            "fieldType": "numberInput",
                            "name": "epi",
                            "label": "Epi-illumination level %",
                            "min": 0,
                            "max": 100,
                            "default": self._epi,
#                            "value": self._epi,
                        },
                        {
                            "fieldType": "numberInput",
                            "name": "trans",
                            "label": "Trans-illumination level %",
                            "min": 0,
                            "max": 100,
                            "default": self._trans,
#                            "value": self._trans,
                        },
                    ],
                }
            ],
        }
        self.add_meta("gui", build_gui(gui_description, self))

    def illumination(self, epi, trans):
        """
        Set illumination intensity level between 0 and 100 (%)

        Args:
            microscope: Microscope object
            epi (int): Intensity level (0-100) for epi-illumination
            trans (int): Intensity level (0-100) for trans-illumination
        """
        microscope = find_component("org.openflexure.microscope")
        sangaboard: Sangaboard = microscope.stage.board
        self._epi = epi
        self._trans = trans

#       sangaboard.query(f"light_intensity_epi {epi}") // old method
        sangaboard.light_control.epi = epi
        logging.info(f"New epi-illumination set to {sangaboard.light_control.epi}")

#       sangaboard.query(f"light_intensity_trans {trans}")  // old method
        sangaboard.light_control.trans = trans
        logging.info(f"New trans-illumination set to {sangaboard.light_control.trans}")



## Extension views
class IlluminationAPIView(ActionView):
    args = {
        "epi": fields.Integer(
            required=True, metadata={"example": 50, "description": "Epi-Illumination Intensity Level (0 to 100)"}
        ),    
        "trans": fields.Integer(
            required=True, metadata={"example": 50, "description": "Trans-Illumination Intensity Level (0 to 100)"}
        ),        
    }

    def post(self, args):      
        # call method to set the new intensity levels
        return self.extension.illumination(args["epi"], args["trans"])

LABTHINGS_EXTENSIONS = (IlluminationExtension,)