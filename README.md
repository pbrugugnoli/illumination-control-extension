# Illumination Control Extension



## Objective
The purpose of this extension is to enable users to control the intensity of epi/trans illumination lights through the OFM GUI interface.

![GUI/API Extension](/images/IlluminationControlExtensionAPI.png)
Figure 1 - User Interface

<br><br>
## Overview
In order to make it work, all 4 layers of the OFM architecture must be adjusted:

1. **GUI/API Extension**:<br>
	This OFM extension, [Illumination Control Extension](https://gitlab.com/pbrugugnoli/illumination-control-extension.git), adds a new menu to the microscope user interface, allowing users to control light intensity via the GUI.

2. **Driver (board library)**:  
	The [pySangaboard fork](https://gitlab.com/pbrugugnoli/pysangaboard) has to be installed, replacing the current pySangaboard library v0.3.3. This fork introduces a new LightControl class as an optional module, exposing light control capabilities to the OFM extensions.

3. **Firmware**:<br> 
	The [Sangaboard fork](https://gitlab.com/pbrugugnoli/sangaboard) has to be installed in the Arduino Nano (Sangaboard alternative). This forked version listens to commands from the serial port (sent by the pySangaboard library) and adjusts the duty cycle of the PWM outputs (analog outputs).

4. **Hardware**:<br> 
	My OFM uses an Arduino Nano as an alternative to the Sangaboard. Two of its analog ports (PWM enabled outputs) should be connected to two LDO6AJSA modules, which serve as PWM controlled current drivers for the illumination LEDs.

Note: The forks may be merged into the main branch if deemed appropriate by maintainers. Otherwise, adjustments will be made to align the hardware with the main branch as they evolve and the required illumination control becames native.


<br><br>

## Implementation
The following sub-sections provide detailed explanations of each layer and the installation procedures, on a bottom-up approach, from the hardware to the GUI/API Extension.

### I. Hardware 
#### 1. Description
LEDs are properly controlled by adjusting the current through them (not voltage), therefore, they require a "current driver", which can be easily built with some transistors/OpAmps, and a variable resistor (for example an analog potentiometer).

Considering the microscpé, however, it's necessary for the Sangaboard (Arduino Nano) to be capable of controlling the current source. This can be achieved either by selecting a resistor (such as a digital resistor like X9Z103P, X9Z104P, or a MCP41010 using the SPI protocol) or by outputting a PWM signal with a variable duty cycle, which is then connected to a low pass filter. Both solutions are viable but require a few electronic components, some soldering, and possibly a small PCB. In such scenarios, opting for the Sangaboard kit/board would be more practical.

The LDO6AJSA module, based on the CN5711 chip, serves as an excellent, cheap and modular alternative. It functions as a current driver module, allowing the current to be adjusted via a PWM signal. Additionally, it features an internal potentiometer for setting the maximum current output of the module.

#### 2. Implementation 
The following perforated board schematic integrates the Arduino Nano board with the LED Current Drivers (LDO5AJSA)* and with the stepper motor drivers (in my case the 2PH124959A - blue modules with the ULN2003 stepper motor drivers).

![Figure 1 - Schematics](/images/OFMControlBoardSchematics.png)
Figure 2 - Control Board Schematics

![Figure 2 - Control Board Alternative](/images/OFMControlBoard.jpg)
Figure 3 - Control Board Alternative

Notes:<br>
(1) My OFM has an interchangeable illumination (trans-illumination and epi-illumination), therefore two current drivers for 2 LEDs (a 3W smd with star heat sink LED for the epi-illumination and a 60mW common thru-hole white LED) were required;<br>
(2) Digital outputs D05 and D06 of the Arduino Nano are connected to the PWM input control ports of the LDO6AJSA;<br>
(3) To neatly connect the stepper motor driver modules (2PH124959A), the original pins were partially desolderred and pushed down - see image above;<br>
(4) The perforated board isn't mandatory, but it offers convenience by reducing cable clutter. Alternatively, the connections depicted in the schematic can be made using a set of Dupont cables.

<br><br>
### II. Firmware (sangaboard.ino)
#### 1. Description
The [Sangaboard fork](https://gitlab.com/pbrugugnoli/sangaboard) modifies the sangaboard.ino program, installed in the Sangaboard/Arduino Nano, to include logic for setting and retrieving light intensity levels via serial port commands.

1. #define statements are used to indicate that the light control is available for epi/trans illumination 
	~~~
	#define LIGHT_CONTROL_EPI
	#define LIGHT_CONTROL_TRANS
	~~~

2.  Variables to define the output pins D05 and D06 and to control the state of the intensity of the LEDs (PWM / analog output) are declared. 
	~~~
	// Light control variables
	// max_epi and max_trans (should be adjusted fro 0 to 255 to limit
	// current to the LEDs)
	// the following variable encode de 0-255 value for the PWM signal 
	// (0 to 100% duty cycle)
	const int epiPin = 6;        //pin D06
	const int transPin = 5;     //pin D05
	const int max_epi= 180;     // adjusted to limit LED current to 580mA 
	const int max_trans = 157;  // adjusted to limit LED current to 20mA 
	int light_intensity_epi = 0;
	int light_intensity_trans = 0;
	~~~

	Using a voltimeter/amperimeter, the maximum allowed PWM duty cycle is determined and the values are stores in the constants max_epi and max_trans, after adjusting the internal potentiometers of the LDO6AJSA modules for the respective LEDs, according to their maximum allowed current (~600 mA for the 3W LED - epi-illumination, and ~20mA for the 60mA LED - trans-illumination)
	
3. Attention: the fork changes the Arduino Nano output ports mapping to simplify the wiring as show in the schematic above.
	~~~~
	motors[0] = new Stepper(8, 12, 11, 10, 9);
	motors[1] = new Stepper(8, 8, 7, 4, 2);
	motors[2] = new Stepper(8, A0, A1, A2, A3);
	~~~~

4. Functions are defined to set / print the light intensity for the epi/trans illumination 
	~~~~
	#ifdef LIGHT_CONTROL_EPI
	void set_light_intensity_epi(int intensity){
	  light_intensity_epi = constrain(intensity, 0, 100);
	  analogWrite(epiPin, map(light_intensity_epi, 0, 100, 0, max_epi));
	}
	void print_light_intensity_epi(){
	  Serial.println(light_intensity_epi);
	}
	#endif // LIGHT_CONTROL_EPI
	#ifdef LIGHT_CONTROL_TRANS
	void set_light_intensity_trans(int intensity){  
	  light_intensity_trans = constrain(intensity, 0, 100);
	  analogWrite(transPin, map(light_intensity_trans, 0, 100, 0, max_trans));
	}
	void print_light_intensity_trans() {
	  Serial.println(light_intensity_trans);
	}
	#endif // LIGHT_CONTROL_TRANS
	~~~~


5. The main loop was changed to:
	- list the  epi/trans illumination when the "list_modules" command is received 
		~~~~
		#if defined(LIGHT_CONTROL_EPI)||defined(LIGHT_CONTROL_TRANS)
		Serial.print("Light Control:");
		#ifdef LIGHT_CONTROL_EPI
		  Serial.print(" epi");
		#endif
		#ifdef LIGHT_CONTROL_TRANS
		  Serial.print(" trans");
		#endif
		Serial.println();
		#endif
		~~~~
	- process the commands received from the serial port (issued manually or by the pySangaboard library)
		~~~~
		#ifdef LIGHT_CONTROL_EPI
		if(command.startsWith("light_intensity_epi?")){
		  print_light_intensity_epi();
		  return;
		}
		if(command.startsWith("light_intensity_epi ")){
		  int preceding_space = command.indexOf(' ',0);
		  if(preceding_space <= 0){
			Serial.println("Bad command.");
			return;
		  }
			  set_light_intensity_epi(command.substring(preceding_space+1).toInt());
		  Serial.print("Epi-illumination intensity set to ");
		  Serial.print(light_intensity_epi);
		  Serial.println("%");
		  return;
		}
		#endif //LIGHT_CONTROL_EPI

		#ifdef LIGHT_CONTROL_TRANS
		if(command.startsWith("light_intensity_trans?")){
		  print_light_intensity_trans();
		  return;
		}
		if(command.startsWith("light_intensity_trans ")){
		  int preceding_space = command.indexOf(' ',0);
		  if(preceding_space <= 0){
			Serial.println("Bad command.");
			return;
		  }
		  set_light_intensity_trans(command.substring(preceding_space+1).toInt());
		  Serial.print("Trans-illumination intensity set to ");
		  Serial.print(light_intensity_trans);
		  Serial.println("%");
		  return;
		}
		#endif //LIGHT_CONTROL_TRANS
		~~~~

#### 2. Installation 
To install the firmware:
1. Download [sangaboard.ino](https://gitlab.com/pbrugugnoli/sangaboard/-/blob/master/arduino_code/sangaboard/sangaboard.ino?ref_type=heads) from the fork;
2. Uncomment/Comment the "#define" statements to activate/deactivate the used LEDs;
3. Adjust port mapping as necessary;
4. Set the value of max_epi and max_trans to 255;
5. Transfer the new code to your board using your preferred method (for example, the Arduino IDE).

#### 3. Configuration
1. Connect each LED to the outputs of the LDO6AJSA modules or to the perforated board/PCB. Additionally, include an ammeter in series with the LED (or a 1 Ohm resistor and a voltmeter in parallel to the resistor) to measure the LED current.
2. The following procedure aims to maximize the range of the PWM duty cycle, identifying the optimum/maximum values for constants max_epi and max_trans, while ensuring the LED current remains within specifications. The example below demonstrates for trans-illumination, but the process applies similarly for epi-illumination:
	1. Set a low intensity value for the light by sending a command to the serial port (via the serial monitor of the Arduino IDE), such as "light_intensity_trans 20";
	2. Using a screwdriver, adjust the internal potentiometer of the LDO6AJSA module to 20% of the maximum current allowed by the LED;
	3. Gradually increase the light intensity by sending new commands through the serial port ("light_intensity_trans X"), and fine-tune the internal potentiometer to achieve a current through the LED that corresponds to X% of the maximum value;
	4. Repeat the previous step until reaching the limit of the potentiometer and unable to further increase X;
	5. For example, if at the end X is 70, it implies that the maximum duty cycle of the PWM associated with the trans-illumination LED should be limited to 0.7 * 255 = 178.5. Hence, update the max_trans to 178 (round it down) and transfer the adjusted code to the board (repeat step 5 of the installation).


<br><br>
### III. Driver (pySangaboard library)
#### 1. Description
The [fork to the pySangaboard v0.3.3](https://gitlab.com/pbrugugnoli/pysangaboard.git) adds a new LightControl class as an optional module, exposing light control capability to the OFM extensions.

The changes in the **sangaboard.py** program was minimal:

1. In the constructor ("_ _ init _ _" ) of the Sangaboard class, the property light_control is created when the "Light Control" model is identified (it returns as part of the response to the "list_modules" command sent to the Sangaboard/Arduino Nano)
	~~~~
	for module in self.list_modules():
	...
		elif module_type.startswith("Light Control"):
			self.light_control = LightControl(True, 
			                                  parent=self, 
			                                  model=module_model)
    ...			                                  
	~~~~

2. The LightControl class utilizes the **QueriedProperty class** from **extensible_serial_instrument.py** to define the epi and trans properties. This class greatly simplifies the process of setting and reading properties by encapsulating queries, parsing, and validating data through serial communication.
~~~
class LightControl(OptionalModule):
    """An optional module to control light intensity for epi and trans-
    illumination.

    If a light controls is enabled in the motor controller's firmware, then
    the :class:`sangaboard.Sangaboard` will gain an optional
    module which is an instance of this class.  It can be used to set and
    get the illumination intensity from 0 to 100 (%)
    """    

    installed = []
    """List of installed light controller (epi, trans)"""

    def __init__(self, available, parent=None, model="Generic"):
        super(LightControl, self).__init__(available, parent=parent, module_type="LightControl", model=model)
        self.installed = model.split(" ")
        logging.info(f"Light control enabled modules in the firmware: {self.installed}") 
     
    epi = QueriedProperty(
        get_cmd="light_intensity_epi?",
        set_cmd="light_intensity_epi %d",
        response_string="%d",
        doc="Get or set the light intensity for the epi-illumination.\n\n"
        "The intensity scale ranges from 0 to 100 (%)",
    )    
    trans = QueriedProperty(
        get_cmd="light_intensity_trans?",
        set_cmd="light_intensity_trans %d",
        response_string="%d",
        doc="Get or set the light intensity for the trans-illumination.\n\n"
        "The intensity scale ranges from 0 to 100 (%)",
    )  
~~~

#### 2. Installation
To install the fork:
~~~~
cd ~
git clone https://gitlab.com/pbrugugnoli/pysangaboard.git
cd pysangaboard 
pip install -e .
~~~~

<br><br>
### IV. API Extension (Illumination Control Extension)
#### 1. Description
The API extension enables users to define epi-illumination and trans-illumination intensity levels through the GUI interface, from 0% to 100% of their maximum power.

Once "SET ILLUMINATION" is pressed the following communication chain is triggered: API Extension ->  sangaboard.LightControl.trans(epi) --serial communication--> sangaboard.ino --PWM duty cycle-->  LDO6AJSA Current Driver.

#### 2. Pending topics (TO DO LIST)
1. When the IlluminationExtension class is instantiated, the microscope instance cannot be found, resulting in an error when attempting to retrieve it using the find_component("org.openflexure.microscope") function. Consequently, the list of installed LEDs from microscope.stage.board.light_control.installed cannot be retrieved to dynamically enable/disable options in the user interface, even when using a dynamic GUI function;
2. The extension based on "[Extension Example](https://gitlab.com/openflexure/microscope-extensions/example-extension)"  did not function as expected. This issue may be related to the fact that the example repository was created under the assumption that the pending OFEP for extensions would be approved. However, it will not function until at least the next release (likely 2.11) of the microscope server.

#### 3. Installation 
To install the microscope extension:
1. Clone the extension repository 
	~~~~
	cd ~
	git clone https://gitlab.com/pbrugugnoli/pysangaboard.git
	~~~~

2. Create a link from the downloaded extension to the microscope_extension directory:
	~~~~
	sudo ln -s /home/<YOUR USER ID>/illumination_control /var/openflexure/extensions/microscope_extensions/
	~~~~
